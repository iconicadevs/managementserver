const mngmntRoutes = require("./mngmntRoutes.js");
const { app, server } = require("./dmr/server.js");
const global = require("./global.js");

app.use("/mngmnt", mngmntRoutes);

global.server = server.listen(8080, () => {});
