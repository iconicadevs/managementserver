const express = require("express");
const router = express.Router();
const global = require("./global.js");

router.get("/shutdown", (req, res) => {
  global.server.close();
  res.send("Shutting down");
  process.exit();
});

module.exports = router;
