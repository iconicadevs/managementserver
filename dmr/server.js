const express = require("express");
const http = require("http");
const app = express();
const server = http.createServer(app);

app.use(express.json());

app.get("/api/v1/configurations/version", (req, res) => {
  res.send({ version: "0.0.0" });
});

module.exports = { app, server };
