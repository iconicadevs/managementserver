const express = require("express");
const https = require("https");
const auth = require("../authorisation.js");
const Updater = require("../updater.js");
const options = require("../generate_certificates.js");
const { tryer, tryerAsync, log, readLog, removeLog } = require("../utils.js");
const exprfup = require("express-fileupload");
const si = require("systeminformation");
const version = require("./config.js");

const app = express();

const server = https.createServer(options, app);

app.use(express.json());
app.use(exprfup({ createParentPath: true }));
app.use(auth());

const updater = new Updater();

app.get("/version", (req, res) => {
  tryer(req, res, () => {
    log("Version requested..");
    res.send(`Version ${version}`);
  });
});
app.get("/log", (req, res) => {
  tryer(req, res, () => {
    res.send(readLog());
  });
});
app.get("/purge", (req, res) => {
  tryer(req, res, () => {
    removeLog();
    res.send("Cleared log");
  });
});

app.get("/restart", async (req, res) => {
  await tryerAsync(req, res, async () => {
    log("Restarting the server...");
    if (await updater.restart()) {
      log("Restart successful...");
      res.send("Restart successful...");
    } else {
      log("Restart unsuccessful");
      res.send("Restart unsuccessful");
    }
  });
});

app.get("/system-info", async (req, res) => {
  const osInfo = await si.osInfo();
  const systemInfo = await si.system();
  const versionInfo = await si.versions();
  const cpuInfo = await si.cpu();
  res.send({
    osInfo,
    systemInfo,
    versionInfo,
    cpuInfo,
  });
});

module.exports = { app, server };
