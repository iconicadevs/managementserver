const { log } = require("./utils.js");

function auth() {
  return function (req, res, next) {
    if (req.headers["authorization"] !== "Bearer d2VfYXJlX2ljb25pY2E=") {
      log("forbidden call");
      res.status(403);
      res.end();
    } else {
      return next();
    }
  };
}

module.exports = auth;
