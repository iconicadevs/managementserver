const Updater = require("./updater.js");
const { tryerAsync, log, isDMRInstalled } = require("./utils.js");
const fs = require("fs");
const { server, app } = require("./management/server.js");
const version = require("./management/config.js");

let args = {
  args: process.argv.slice(2),
};

fs.writeFile("../args.json", JSON.stringify(args), "utf8", () => {});

const updater = new Updater();
setTimeout(() => {
  // give the mongod service a second to start up...
  updater.startServer();
}, 25000);


const updateServer = (server) => async (req, res) => {
  (await tryerAsync(
    req,
    res,
    async () => await updater.update(req.files, `${server}`)
  ))
    ? log("update successful...")
    : log("update unsuccessful");
};

app.post("/update", updateServer("../dmr"));

app.post("/update/management", updateServer("./management"));

server.listen(8085, () => {
  console.log(`Management Server ${version} Up and running`);
  if (!isDMRInstalled()) console.log("DMR Server not installed yet");
});
