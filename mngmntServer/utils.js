const fs = require("fs");
const fse = require("fs-extra");

const logfile = `${__dirname}/_log.txt`;

function log(line) {
  if (fs.existsSync(logfile) && fs.statSync(logfile).size > 1024 * 1024)
    fse.removeSync(logfile);
  fs.appendFileSync(logfile, Date() + ": " + line + "\r\n");
}
function removeLog(log = logfile) {
  if (fs.existsSync(log)) fse.removeSync(log);
}
function readLog(log = logfile) {
  if (fs.existsSync(log)) return fs.readFileSync(log);
  return "";
}
function tryer(req, res, cb, ...args) {
  let status = 200;
  try {
    cb(...args);
  } catch (e) {
    log(e.message + "\r\n" + e.stack);
    status = 500;
  } finally {
    res.status(status);
    res.end(
      status == 200
        ? ""
        : "Something has gone wrong, check the logs to see the failure"
    );
    return status == 200;
  }
}
async function tryerAsync(_, res, cb, ...args) {
  let status = 200;
  try {
    await cb(...args);
  } catch (e) {
    status = 500;
    log(e.message);
  } finally {
    res.status(status);
    res.end(
      status == 200
        ? ""
        : "Something has gone wrong, check the logs to see the failure"
    );
    return status == 200;
  }
}

const isDMRInstalled = () =>
  fs.readdirSync("../dmr").filter((f) => !f.startsWith(".")).length > 1;

module.exports = {
  logfile,
  log,
  removeLog,
  readLog,
  tryer,
  tryerAsync,
  isDMRInstalled,
};
