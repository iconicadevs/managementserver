const fetch = require("node-fetch");
const fs = require("fs");
const fse = require("fs-extra");
const extract = require("extract-zip");
const path = require("path");
const { log } = require("./utils.js");
const { execSync, exec } = require("child_process");
const { stdout, stderr } = require("process");

class Updater {
  async restart() {
    log("Restarting the server");
    try {
      // just try to stop the server if it was started.. otherwise, nothing...
      await this.stopServer();
    } catch (e) {
      log(e);
    }
    await this.startServer();
    log("Restarting completed");
    return true;
  }

  async update(files, dest) {
    log("Updating the server");
    log("validating the payload");
    if (files["zipfile"].name.indexOf(".zip") < 0) throw "Invalid payload";
    try {
      // just try to stop the server if it was started.. otherwise, nothing...
      await this.stopServer();
    } catch (e) {
      log(e);
    }
    log("Copying the uploaded files to upload directory ");
    for (let f in files) files[f].mv("./upload/" + files[f].name);
    return this.extractAndCopy("./upload", dest);
  }

  async extractAndCopy(folder, dest) {
    return new Promise((res, rej) =>
      setTimeout(async () => {
        for (let uploadedfile of fs.readdirSync(folder)) {
          if (uploadedfile.indexOf(".zip") > 0) {
            log("Extracting zip file " + uploadedfile);
            await extract(folder + "/" + uploadedfile, {
              dir: `${path.join(__dirname + folder.substring(1) + "/extract")}`,
            });
          }
        }
        await new Promise((res, rej) => setTimeout(() => res(), 500));
        if (!fs.existsSync(`${folder}/extract/`)) {
          log(`Nothing to extract to ${dest}, done....`);
          res(true);
          return;
        }
        log(`Moving extracted files from " + folder + "/extract to ${dest}`);
        fse.moveSync(`${folder}/extract/`, `${dest}`, { overwrite: true });
        log("Removing temp files and folders");
        fse.removeSync(`${folder}/extract`, { recursive: true, force: true });
        fse.removeSync(`./upload`, { recursive: true, force: true });
        try {
          await this.startServer(
            dest == "../dmr"
              ? "cd .. && node server.js"
              : "sudo systemctl restart dmr.service"
          );
          log("Updating completed");
          res(true);
        } catch (e) {
          log("Updating failed");
          rej(e);
        }
      }, 500)
    );
  }

  async stopServer() {
    log("Shutting down backend");
    await fetch("http://localhost:8080/mngmnt/shutdown");
    log("Shutted down backend");
  }

  async startServer(command = "cd .. && node server.js") {
    let err = null;
    exec(command, (error, stdout, stderr) => {
      err = error;
    });
    return new Promise((res, rej) => {
      setTimeout(() => {
        if (err) {
          rej(err);
        } else {
          res(true);
        }
      }, 500);
    });
  }
}

module.exports = Updater;
