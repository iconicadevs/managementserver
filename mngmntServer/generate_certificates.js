const fs = require("fs");
const process = require("child_process");
const certfile = "selfsigned.crt";
const keyfile = "selfsigned.key";
const path = "/certs/";

process.execSync(
  `sudo openssl req -x509 -nodes -days 6000 -newkey rsa:2048 -config .${path}req.cnf -keyout .${
    path + keyfile
  } -out .${path + certfile}`
);
let options = {
  key: fs.readFileSync(__dirname + path + keyfile),
  cert: fs.readFileSync(__dirname + path + certfile),
};

module.exports = options;
