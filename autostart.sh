#!/bin/sh
rm -f /data/db/mongod.lock
rm -f /var/lib/mongodb/mongod.lock
killall mongod
mongod --repair
mongod --journal &
cd /usr/src/app/mngmntServer
node mngmntServer.js